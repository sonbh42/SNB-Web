import variable from "./../variables";

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    ".note": {
      color: "#a7a7a7",
      fontSize: variables.noteFontSize
    },
    '.size9': {
      fontSize: 9,
    },
    '.size10': {
      fontSize: 10,
    },
    '.size11': {
      fontSize: 11,
    },
    '.size12': {
      fontSize: 12,
    },
    '.size13': {
      fontSize: 13,
    },
    '.size20': {
      fontSize: 20,
    },
    '.size35': {
      fontSize: 35,
    },
    '.semi': {
      fontFamily: 'OpenSans-SemiBold'
    },
    '.light': {
      fontFamily: 'OpenSans-Light'
    },
    '.bold': {
      fontFamily: 'OpenSans-Bold'
    },
    '.white': {
      color: '#ffffff',
    }
  };

  return textTheme;
};
