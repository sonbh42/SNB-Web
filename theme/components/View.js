import variable from "./../variables";

export default (variables = variable) => {
  const viewTheme = {
    ".padder": {
      padding: variables.contentPadding
    },
    '.center': {
      justifyContent: 'center',
      alignItems: 'center'
    },
    '.flexEnd': {
      justifyContent:'center',
      alignItems: 'flex-end',
    },
    '.padder': {
      padding: variables.contentPadding
    },
    '.full': {
      justifyContent: 'space-between',
      width: '100%',
    },
    '.row': {
      flexDirection: 'row',
      alignItems: 'center',
    },
    '.flexStart': {
      justifyContent: 'center',
      flex: 1,
      padding: 15
    },
    '.align':{
      alignItems: 'center'
    }

  };

  return viewTheme;
};
