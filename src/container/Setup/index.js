import React, { Component } from 'react';
import { } from 'react-native';
import { Text, View, Container } from 'native-base';
import {connect} from 'react-redux'

import Toolbar from '../../component/Toolbar';
import AuthButton from '../../component/AuthButton';
import { logout } from '../../store/actions/auth';


@connect(state => ({

}),
  {
    logout
  }
)

class Setup extends Component {
  render() {
    return (
      <Container>
        <Toolbar
          leftPress={() => { this.props.navigation.openDrawer() }}
          typeLeft='icon'
          iconLeft='menu'
          title='Thiết lập' />
        <AuthButton
          onPress={() => {
            this.props.logout()
            
            this.props.navigation.navigate('AuthStack')
          }}
          text="Đăng Xuất" />
      </Container>
    )
  }
}

export default Setup