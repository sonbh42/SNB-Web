import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Text ,View} from 'native-base';


import Toolbar from '../../component/Toolbar';
import SearchBar from '../../component/SearchBar'

class Credit extends Component {
  render() {
    return (
      <Container>
        <Toolbar
          leftPress={() => { this.props.navigation.openDrawer() }}
          typeLeft='icon'
          iconLeft='menu'
          title='Quản lý tín dụng' />
        <View style={styles.toolbar} >
          <SearchBar
            onPress={() => { this.props.navigation.navigate('Search') }}
            touch
            placeholder='Tìm kiếm' />
        </View>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  toolbar: {
    // height: variables.toolbarHeight,
    height: 45,
    backgroundColor: '#4B4B61',
    justifyContent: 'center',
    padding: 5,
  }
})
export default Credit