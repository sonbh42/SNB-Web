import React, { Component } from 'react'
import {
    Image,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import { View, Text } from 'native-base';
import Permissions from 'react-native-permissions';
import LinearGradient from 'react-native-linear-gradient';

import { verifyToken } from '../../store/actions/auth'
import images from '../../assets/images'

@connect(state => ({
    token: state.auth.token,
    loggedIn: state.auth.loggedIn,
}),
    {
        verifyToken
    })
class Splash extends Component {
    constructor(props) {
        super(props)
        this.state = {
            locationPermission: {}
        }
    }
    componentDidMount() {
        setTimeout(this.loadApp, 600);
    }
    _requestPermission = () => {
        Permissions.request('location')
            .then(response => {
                this.setState({ locationPermission: response })
            });
    }
    loadApp = async () => {
        const { token, loggedIn, navigation } = this.props;
        if (loggedIn && token) {
            this.props.verifyToken(token, (error, data) => {
                if(!error && data){
                    if(data.code ===200){
                        navigation.navigate('AppDrawer')
                    }else {
                        navigation.navigate('AuthStack')
                    }
                }else {
                    navigation.navigate('AuthStack')
                }
            })
        }else {
            navigation.navigate('AuthStack')
        }
    }
    render() {
        return (
            <LinearGradient colors={['#FF945D', "#FF5700"]} style={styles.linerarGradient}>
                <View center style={{ flex: 1 }}>
                    <Image source={images.splashLogo} style={{ marginTop: 10 }} color='white' />
                    <ActivityIndicator style={{ marginTop: 10 }} color='white'/>
                </View>
            </LinearGradient>
        )
    }
}
const styles = StyleSheet.create({
    linerarGradient: {
        flex: 1,
    }
})
export default Splash
