import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  RefreshControl
} from 'react-native';
import { Container, Separator } from 'native-base';

import SearchBar from '../../component/SearchBar';
import ListCustomer from '../Customer/ListCustomer'
import Toolbar from '../../component/Toolbar'

class Customer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    }
  }
  renderScrollViewContent() {
    return (
      <View>
        <Separator style={{ height: 10 }} />
        <ListCustomer navigation={this.props.navigation} />
      </View>
    )
  }
  render() {
    return (
      <Container>
        <Toolbar
          leftPress={() => { this.props.navigation.openDrawer() }}
          typeLeft='icon'
          iconLeft='menu'
          title='Khách hàng' />
        <View style={styles.toolbar} >
          <SearchBar
            onPress={() => { this.props.navigation.navigate('Search') }}
            touch
            placeholder='Tìm kiếm khách hàng' />
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl colors={["#F55807", "#F55807", "blue"]} refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                setTimeout(() => this.setState({ refreshing: false }), 1000);
              }} />
          }>
          {this.renderScrollViewContent()}

        </ScrollView>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  toolbar: {
    // height: variables.toolbarHeight,
    height: 45,
    backgroundColor: '#4B4B61',
    justifyContent: 'center',
    padding: 5,
  }
})
export default Customer