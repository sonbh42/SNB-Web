import React, { Component } from 'react';
import { FlatList, TouchableOpacity } from 'react-native';
import { View, Text } from 'native-base';
import { connect } from 'react-redux';

import { getToken } from '../../../store/selectors/auth';
import { getShopCustomer } from '../../../store/actions/ecommerce';
import Customer from '../../../component/Customer';
@connect(state => ({
    token: getToken(state)
}), {
        getShopCustomer,
    }
)

export class ListCustomer extends Component {
    state = {
        customers: []
    }
    componentDidMount() {

        this.props.getShopCustomer(this.props.token, (error, data) => {
            if (error) return;
            this.setState({ customers: data.data })
        })
    }
    keyExtractor = (item ,index) => item._id
    renderContent() {
        return (
            <View white>
                <View padder full row>
                    <Text size12 style={{ color: '#2E3857' }}>DANH SÁCH KHÁCH HÀNG</Text>
                </View>
                <View>
                    <FlatList
                        data={this.state.customers}
                        extraData={this.state}
                        keyExtractor={this.keyExtractor}
                        renderItem={({ item, index }) => <Customer item={item} navigation={this.props.navigation}
                            containerStyle={{
                                marginRight: 5,
                                marginLeft: index === 0 ? 0 : 5
                            }} />}
                    />
                </View>
            </View>
        )
    }
    render() {
        return (
            this.state.customers ? this.renderContent() : null
        )
    }
}

export default ListCustomer