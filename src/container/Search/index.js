import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { View, Text, Container } from 'native-base';
import { connect } from 'react-redux';


import Toolbar, { ToolbarIcon } from '../../component/Toolbar'
import { SearchBar } from '../../component/SearchBar';
import Product from '../../component/Product';
import { getToken, isRequestPending } from '../../store/selectors';
import variable from '../../../theme/variables';
import { searchProduct } from '../../store/actions';
const width = (variable.deviceWidth - 30) / 3;

connect(state => ({
    token: getToken(state),
    loadingSearchProduct: isRequestPending(state, 'searchProduct')
}),
    {
        searchProduct
    })
class Search extends Component {
    state = {
        products: [],
    }
    onSearch = () => {
        const { token, searchProduct } = this.props;
        searchProduct(token, text, 1, 100, (error, data) => {
            if (error) return;
            console.log('search', data);
            this.setState({ products: data.data });
        });
    }
    render() {
        return (
            <Container white>
                <Toolbar>
                    <View row>
                        <ToolbarIcon
                            onPress={() => this.props.navigation.goBack()}
                            type='white'
                            icon={'arrow-back'} />
                        <SearchBar
                            onSearch={this.onSearch}
                            containerStyle={{ marginRight: 10 }} />
                    </View>
                </Toolbar>
                <View row style={{ paddingHorizontal: 10, paddingVertical: 10, flex: 1 }}>
                    <FlatList
                        refreshing={this.props.loadingSearchProduct}
                        data={this.state.products}
                        extraData={this.state}
                        numColumns={3}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item, index }) => <Product item={item} width={width}
                            containerStyle={{
                                marginHorizontal: index % 3 === 1 ? 5 : 0,
                            }} />}
                    />
                </View>
            </Container>
        )
    }
}

export default Search
