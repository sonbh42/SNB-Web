import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import { Container, Text, View } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux'


import AuthInput from '../../../component/AuthInput';
import AuthButton from '../../../component/AuthButton';
import AppStyle from '../../../theme/AppStyle';
import * as authActions from '../../../store/actions/auth';
import { isRequestPending } from '../../../store/selectors/common';
import { checkValidMail, checkValidPassword } from '../../../utils/validate';

@connect(
    state => ({
        auth: state.auth,
        loading: isRequestPending(state, 'login'),
    }),
    {
        login: authActions.login
    }
)
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'provider1@twinger.co',
            password: 'twinger.co@SNB2018'
        }
    }
    validateField = (email, password) => {
        if (email === '' || password === '') {
            Alert.alert(
                'Thông báo',
                `Vui lòng điền email và password `, [{ text: 'Đồng ý' }],
                { cancelable: false }
            );
            return false;
        }
        const errorEmail = checkValidMail(email);
        let errorPassword = checkValidPassword(password);
        if (errorPassword) errorPassword = 'Mật khẩu không đúng'
        if (errorEmail || errorPassword) {
            Alert.alert(
                'Thông báo ',
                `${errorEmail || errorPassword}.Vui lòng thử lại`, [{ text: 'Đồng ý' }],
                { cancelable: false }
            );
            return false;
        }
        if (!errorPassword && !errorEmail) return true;
    }
    goLogin = async (values) => {
        const infoInput = {};
        if (this.validateField(values.email, values.password)) {
            infoInput.email = values.email;
            infoInput.password = values.password;
            infoInput.deviceToken = '123';
            infoInput.deviceId = '456';
            infoInput.deviceType = 'web';
            this.props.login(infoInput, async (error, data) => {
                console.log(data);

                if (data && data.code >= 200 & data.code <= 304) {
                    this.props.navigation.navigate('AppDrawer');
                } else {
                    Alert.alert(
                        'Thông báo',
                        `Xảy ra lỗi ${error.message}`,
                        [{ text: 'Đồng ý' }], { cancelable: false }
                    );
                }
            })
        }
    }
    render() {
        return (
            <LinearGradient colors={['#FF945D', '#FF5700']} style={AppStyle.linearGradient}>
                <View style={AppStyle.authMargin}>
                    <Text light style={AppStyle.h1}>Đăng nhập</Text>
                </View>
                <View>
                    <AuthInput
                        autoCapitalize='none'
                         value='provider1@twinger.co'
                        onChangeText={(value) => {
                            this.setState({ email: value })
                        }}
                        placeholder='Email' />
                    <AuthInput
                        autoCapitalize='none'
                         value='twinger.co@SNB2018'
                        onChangeText={(value) => {
                            this.setState({ password: value })
                        }}
                        placeholder='Mật khẩu' />

                    <View padder flexEnd>
                        <TouchableOpacity>
                            <Text white semi size13>Quên mật khẩu ?</Text>
                        </TouchableOpacity>
                    </View>
                    <AuthButton
                        loading={this.props.loading}
                        onPress={() => this.goLogin({ email: this.state.email, password: this.state.password })}
                        text='ĐĂNG NHẬP' />
                    
                    <View padder row center>
                        <Text white light size12>Chưa có tài khoản?</Text>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Register') }}>
                            <Text size12 white semi> Đăng ký</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        )
    }
}
export default Login