import React, { Component } from 'react';
import { TouchableOpacity, Image, Alert } from 'react-native';
import { View, Text, Container, Icon, Content } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker'

import AuthInput from '../../../component/AuthInput';
import AuthButton from '../../../component/AuthButton';;
import AppStyle from '../../../theme/AppStyle';
import images from '../../../assets/images';
import { checkEmail, checkPassword } from '../../../utils';

const imagePickerOptions = {
    title: 'Chọn ảnh',
    takePhotoButtonTitle: 'Chụp ảnh',
    chooseFromLibraryButtonTitle: 'Chọn từ thư viện ảnh',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
}


class Register extends Component {
    state = {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        imageLoading: ''
    };
    text = {
        name: null,
        email :null,
        password: null,
        confirmPassword: null,
        confirmPassword: null
    }
    chooseImage = () => {
        this.setState({ imageLoading: true })
        ImagePicker.showImagePicker(imagePickerOptions, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.setState({
                    imageSource: source
                });
            }
        });
    }
    showAlert = (message) => {
        Alert.alert(
            'Thông báo',
            `${message}`, [{ text: 'Đồng ý' }]
        )
    }
    validateField = {}
    onNext = () => {
        const { name, email, password, confirmPassword, imageSource } = this.state;
        if (!imageSource) {
            this.showAlert('Vui lòng chọn ảnh đại diện của bạn');
            return;
        }
        if (name && email && password && confirmPassword) {
            if (!checkEmail(email)) {
                this.showAlert('Định dạng email không đúng');
                return;
            }
            if (!checkPassword(password)) {
                this.showAlert('Mật khẩu phải có ít nhât 8 kí tự, 1 kí tự in hoa, 1 kí tự số, 1 kí tự thường');
                return;
            }
            if (password !== confirmPassword) {
                this.showAlert('Xác nhận mật khẩu không khớp');
                return;
            } else {
                this.props.navigation.navigate('RegisterStort',
                    { name, email, password, imageSource });
                return;
            }
        } else {
            this.showAlert('Vui lòng chọn điền đầy đủ thông tin');
            return;
        }
    }
    render() {
        return (
            <LinearGradient colors={['#FF945D', '#FF5700']} style={AppStyle.linearGradient}>
                <Container trans>
                    <TouchableOpacity
                        style={AppStyle.back}
                        onPress={() => { this.props.navigation.goBack() }}>
                        <Icon name="arrow-back" />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: 'space-between' }}>
                        <Content style={AppStyle.authMargin} showsVerticalScrollIndicator={false}>
                            <View row full>
                                <Text light style={[AppStyle.h1, { marginTop: 0 }]}>Đăng ký</Text>
                                <View>
                                    <Text white light size20>bước</Text>
                                    <Text white bold size35>1 <Text white light size20>/</Text> <Text bold white size20 >2</Text></Text>
                                </View>
                            </View>
                            <View align row style={{ marginTop: 20 }}>
                                <TouchableOpacity onPress={this.chooseImage}>
                                    <Image style ={{height:80,width:80,borderRadius:40}} source = {this.state.imageSource ?this.state.imageSource: images.changeAvata}></Image>
                                </TouchableOpacity>
                                <Text light white size12 style={{ marginLeft: 10 }}>Cập nhập ảnh đại diện</Text>
                            </View>
                            <View>
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.name = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ name: value })
                                    }}
                                    onSubmitEditing={() => {
                                        this.text.email.focus();
                                    }}
                                    placeholder='Họ tên' />
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.email = ref)}
                                    keyboardType='email-address'
                                    onSubmitEditing={() => {
                                        this.text.password.focus();
                                    }}
                                    onChangeText={(value) => {
                                        this.setState({ email: value })
                                    }}
                                    placeholder='Email' />
                                <AuthInput
                                    autoCapitalize="none"
                                    secureTextEntry={true}
                                    ref={ref => (this.text.password = ref)}
                                    onSubmitEditing={() => {
                                        this.text.confirmPassword.focus();
                                    }}
                                    onChangeText={(value) => {
                                        this.setState({ password: value })
                                    }}
                                    placeholder='Mật khẩu' />
                                <AuthInput
                                    autoCapitalize="none"
                                    secureTextEntry={true}
                                    ref={ref => (this.text.confirmPassword = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ confirmPassword: value })
                                    }}
                                    placeholder='Xác nhận mật khẩu' />
                            </View>
                        </Content>
                        <AuthButton
                            // loading={this.state.imageLoading}
                            onPress={() => { this.onNext() }}
                            text="TIẾP THEO" />
                    </View>
                </Container>
            </LinearGradient>
        )
    }
}
// const styles = StyleSheet.create({
//     back: {
//         position: 'absolute',
//         zIndex: 100,
//     }
// });
export default Register