import React, { Component } from 'react';
import { TouchableOpacity, Alert } from 'react-native';
import { View, Text, Icon, Container, Content } from 'native-base';
import { connect } from 'react-redux'

import AuthInput from '../../../component/AuthInput';
import AuthButton from '../../../component/AuthButton';
import LinearGradient from 'react-native-linear-gradient';
import AppStyle from '../../../../src/theme/AppStyle';

import * as authActions from '../../../store/actions/auth';
import { files } from '../../../store/actions/files';
import { isRequestPending } from '../../../store/selectors/common';
import { checkPhone, validBlankField } from '../../../utils/validate';

@connect(state => ({
    loadingRegister: isRequestPending(state, 'register'),

}), {
        register: authActions.register,

    })
export class RegisterStort extends Component {
    state = {
        customer: '',
        phone: '',
        discription: '',
        address: '',
    }
    text = {
        customer: null,
        phone: null,
        discription: null,
        address: null
    }
    showAlert = (message) => {
        Alert.alert(
            'Thông báo',
            `${message}`, [{ text: 'Đồng ý' }]
        )
    }

    register = () => {
        const { name, email, password, imageSource } = this.props.navigation.state.params;
        const { customer, phone, discription, address } = this.state;

        if (customer && phone && discription && address) {
            if(!checkPhone(phone)){
                this.showAlert('Định dạng số điện thoại không đúng');
                return;
            }else{}
        } else {
            this.showAlert('Vui lòng chọn điền đầy đủ thông tin');
            return;
        }
        let shopInfo = {
            name:this.state.customer,
            description: this.state.discription,
            phone: this.state.phone,
            address: this.state.address
        }
        this.props.register({
            name,
            email,
            password,
            phone,
            shopInfo
        })
    }
    render() {
        console.log('FILE', this.props.navigation.state.params);
        return (
            <LinearGradient colors={['#FF945D', '#FF5700']} style={AppStyle.linearGradient} >
                <Container trans>
                    <TouchableOpacity
                        style={AppStyle.back}
                        onPress={() => { this.props.navigation.goBack() }}>
                        <Icon name="arrow-back" />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: 'space-between' }}>
                        <Content showsVerticalScrollIndicator={false} style={AppStyle.authMargin}>
                            <View row full>
                                <Text light style={[AppStyle.h1, { marginTop: 0 }]}>Đăng ký</Text>
                                <View>
                                    <Text size20 light white>bước</Text>
                                    <Text white bold size35>2 <Text light white size20>/</Text><Text light size20 white>2</Text></Text>
                                </View>
                            </View>
                            <View >
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.customer = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ customer: value })
                                    }}
                                    onSubmitEditing={() => {
                                        this.text.phone.focus();
                                    }}
                                    placeholder='Tên cửa hàng' />
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.phone = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ phone: value })
                                    }}
                                    onSubmitEditing={() => {
                                        this.text.discription.focus();
                                    }}
                                    keyboardType='phone-pad'
                                    placeholder='Số điện thoại ' />
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.discription = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ discription: value })
                                    }}
                                    onSubmitEditing={() => {
                                        this.text.address.focus();
                                    }}
                                    placeholder='Mô tả ' />
                                <AuthInput
                                    autoCapitalize="none"
                                    ref={ref => (this.text.address = ref)}
                                    onChangeText={(value) => {
                                        this.setState({ address: value })
                                    }}
                                    placeholder='Địa chỉ gian hàng' />
                            </View>
                        </Content>
                        <AuthButton
                            loading={this.props.loadingRegister}
                            onPress={this.register}
                            text="ĐĂNG KÝ" />
                    </View>
                </Container>
            </LinearGradient>
        )
    }
}
export default RegisterStort