import React, { Component } from 'react';
import { StyleSheet, Platform, ScrollView, RefreshControl } from 'react-native';
import { View, Text, Container, Content, Separator } from 'native-base';

import variables from '../../../theme/variables';
import Toolbar from '../../component/Toolbar';
import SearchBar from '../../component/SearchBar';
import ListCategory from '../Goods/ListCategory';

const HEADER_MAX_HEIGHT = variables.deviceHeight / 2;

class Goods extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    }
  }
  renderScrollViewContent() {
    return (
      <View>
        {/* style={styles.scrollViewContent} */}
        <Separator style={{ height: 10 }} />
        <ListCategory />
        <Separator style={{ height: 10 }} />
        
        

      </View>
    )
  }
  render() {
    return (
      <Container>
        <Toolbar
          leftPress={() => { this.props.navigation.openDrawer() }}
          typeLeft='icon'
          iconLeft='menu'
          title='Danh sách sản phẩm' />
        <View style={styles.toolbar} >
          <SearchBar
            onPress={() => { this.props.navigation.navigate('Search') }}
            touch
            placeholder='Tìm kiếm sản phẩm' />
        </View>


        <ScrollView
          refreshControl={
            <RefreshControl colors={["#F55807", "#F55807", "blue"]} refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                setTimeout(() => this.setState({ refreshing: false }), 1000);
              }} />
          }>
          {this.renderScrollViewContent()}
        </ScrollView>

      </Container>
    )
  }
}
const styles = StyleSheet.create({
  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    backgroundColor: 'red'
  },
  toolbar: {
    // height: variables.toolbarHeight,
    height: 45,
    backgroundColor: '#4B4B61',
    justifyContent: 'center',
    padding: 5,
  }
})

export default Goods