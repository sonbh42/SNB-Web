import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { Container, View, Text } from 'native-base';

import Product from '../../../component/Category';
import variable from '../../../../theme/variables';

const width = (variable.deviceWidth - 30) / 3;
import { connect } from 'react-redux';
import { getToken } from '../../../store/selectors/auth';
import { getListShopCategory } from '../../../store/actions/ecommerce';
import Category from '../../../component/Category';
@connect(state => ({
    token: getToken(state)
}), {
        getListShopCategory,
    }
)
class ListCategory extends Component {
    state = {
        category: []
    }
    componentDidMount() {
        this.props.getListShopCategory(this.props.token, (error, data) => {
            console.log ("ddg",data)
            if (error) return;
            this.setState({ category: data.data })
        })
    }
    keyExtractor = (item, index) => item._id
    renderContent() {
        return (
            <View white >
                <View padder full row>
                    <Text size12 style={{ color: '#2E3857' }}>DANH MỤC NGHÀNH HÀNG</Text>
                    <TouchableOpacity>
                        <Text size11 style={{ color: '#3498DB' }}>Xem thêm</Text>
                    </TouchableOpacity>
                </View>

                <View row style={{ paddingHorizontal: 10 }}>
                    <FlatList
                        data={this.state.category}
                        
                        extraData={this.state}
                        horizontal
                        keyExtractor={this.keyExtractor}
                        renderItem={({ item, index }) => <Category
                            item={item}
                            navigation={this.props.navigation}
                            width={width} containerStyle={{
                                marginRight: 5,
                                marginLeft: index === 0 ? 0 : 5
                            }} />}
                    />
                </View>
            </View>
        );
    }
    render() {
        return (
            this.state.category ? this.renderContent() : null
            
        )
    }
}
export default ListCategory;