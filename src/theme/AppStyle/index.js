import {Dimensions,StyleSheet} from 'react-native'
const {height,width} = Dimensions.get('window')
export default {
    linearGradient:{
        flex: 1,
        padding: 30,
        justifyContent: 'space-between',
    },
    authMargin: {
        marginTop: height / 10
    },
    h1: {
        color: 'white',
        fontSize: 35,
    },
    back :{
        position: 'absolute',
        zIndex: 100,
        
    },
    

}