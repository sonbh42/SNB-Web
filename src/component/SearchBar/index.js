import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { View, Text, Icon, Input } from 'native-base';
import _ from 'lodash';

import variables from '../../../theme/variables'
export class SearchBar extends Component {
    state = {}
    onSearch = (text) => {
        this.props.onSearch(text)
    }
    searchDebounce = _.debounce(this.onSearch, 500)
    renderView() {
        return (
            <View style={[styles.searchBox, { ...this.props.containerStyle }]}>
                <Icon name="ios-search" style={styles.icon} />
                <Input
                    onChangeText={(text) => this.searchDebounce(text)}
                    placeholder={this.props.placeholder}
                    style={styles.input}>
                </Input>
            </View>
        )
    }
    renderTouch() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.searchBox, { ...this.props.containerStyle }]}>
                <Icon name="ios-search" style={styles.icon} />
                <Text style={[styles.input, { marginLeft: 5 }]}>{this.props.placeholder}</Text>

            </TouchableOpacity>
        )
    }
    render() {
        return (
            // <View style={styles.toolbar} >
                <View style={[styles.searchBox ,{ ...this.props.containerStyle }]}>
                    {this.props.touch ? this.renderTouch() : this.renderView()}
                {/* </View> */}
            </View>
        )
    }
}
SearchBar.defaultProps = {
    
    containerStyle: {},
    touch: false,
    onPress: () => { }
}
const styles = StyleSheet.create({
    toolbar: {
        height: variables.toolbarHeight,
        backgroundColor: '#4B4B61',
        justifyContent: 'center'

    },
    searchBox: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 20,
        padding: 5,
        height: 28,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'white',
        flex: 1,
    },
    icon: {
        fontSize: 15,
        color: variables.toolbarDefaultBorder
    },
    input: {
        color: variables.toolbarDefaultBorder,
        fontSize: 13,
        
    }
})

export default SearchBar