import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { View, Text } from 'native-base';
import StarRating from 'react-native-star-rating';

import images from '../../assets/images';
import variable from '../../../theme/variables';
import { createImageLink } from '../../utils';
import numeral from 'numeral';

export default class Product extends Component {
    static defaultProps = {
        containerStyle: {},
        width: variable.deviceWidth / 3,
    }
    render() {
        const { item } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('', { item })}
                style={[{ justifyContent: 'center', width: this.props.width, }, { ...this.props.containerStyle }]}>
                <Image
                    style={{ width: this.props.width, height: this.props.width }}
                    source={item.images && item.images.length > 0 ? { uri: createImageLink('ecommerce', item.images[0]) } : images.demo_product} />
                <View padder white style={{ height: 75 }}>
                    <Text
                        light
                        size10
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={{ color: '#2E3857' }}>{item.name}</Text>
                    <Text
                        size10
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={{ color: '#ED5447' }}>{numeral(item.price).format()} ₫</Text>
                    {(item.realPrice && item.realPrice > 0) ?
                        <View row>
                            <Text size9
                                style={{ textDecorationLine: 'line-through', color: '#2E3857' }}>{numeral(item.realPrice).format()} ₫</Text>
                            <Text size10 semi style={{ marginLeft: 5, }}>-{Math.round(((item.realPrice - item.price) / item.realPrice) * 100) / 100}%</Text>
                        </View> : null}
                    {item.rate ? <View style={{ width: 45 }}>
                        <StarRating
                            disabled
                            maxStars={5}
                            rating={item.rate}
                            starSize={9}
                            fullStarColor={'#F7A700'}
                        />
                    </View> : null}
                </View>
            </TouchableOpacity>
        );
    }
}