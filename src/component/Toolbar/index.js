import React, { Component } from 'react';
import { StyleSheet, Platform, TouchableOpacity, Image } from 'react-native';
import { View, Text, Icon } from 'native-base';

import variables from '../../../theme/variables';

export const ToolbarIcon = (props) => (
    <TouchableOpacity
        style={styles.touch}
        onPress={props.onPress}>
        {props.type === 'image' ? <Image style={styles.image} source={props.icon} resizeMode='contain' />
            : <Icon style={styles.icon} color='white' name={props.icon} />}
    </TouchableOpacity>
)

class Toolbar extends Component {
    leftPress = () => {
        if (this.props.leftPress) {
            this.props.leftPress();
        }
    }
    renderContent() {
        return (<View style={styles.toolbarContent}>
            <View style={styles.touch}>
                {this.props.iconLeft &&
                    <ToolbarIcon onPress={this.leftPress} type={this.props.typeLeft} icon={this.props.iconLeft} />}
            </View>
            {this.props.titleComponent ||
                <View>
                    <Text style={styles.toolbarTitle}>{this.props.title}</Text>
                </View>}
            <View style={styles.touch}>
                {
                    this.props.iconRight &&
                    <ToolbarIcon
                        onPress={this.rightPress}
                        type={this.props.typeRight}
                        icon={this.props.iconRight}
                    />
                }
            </View>
        </View>)

    }
    render() {
        return (
            <View style={styles.toolbar}>
                {this.props.children ?
                    <View style={styles.toolbarContent}>
                        {this.props.children}
                    </View>
                    : this.renderContent()
                }
            </View>
        )
    }
}
Toolbar.defaultProps = {
    typeLeft: 'icon',
}
const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: variables.toolbarDefaultBorder,
        height: variables.toolbarHeight
    },
    toolbarContent: {
        marginTop: Platform.OS === 'ios' ? 20 : 0,
        // padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        flex: 1
    },
    touch: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',

    },
    image: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        fontSize: 22,
    },
    toolbarTitle: {
        color: 'white',
        fontSize: 14,
    }
})
export default Toolbar