import React, { Component } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { View, Text } from 'native-base';

import { createImageLink } from '../../utils';
import images from '../../assets/images'

export class Customer extends Component {
    render() {
        const item = this.props.item;
        return (
            <TouchableOpacity>
                <View style ={{flexDirection :'row', padding :12}}>
                    <Image style={{ height: 30, width: 30, borderRadius: 40 }}
                        source={this.props.item.image ?
                            { uri: createImageLink('ecommerce', this.props.item.image) } : images.demo_avatar} />
                    <View style = {{justifyContent:"center" ,marginLeft :20}} white >
                        <Text size10
                            ellipsizeMode='tail'
                            style={{ color: '#2E3857' }}>
                            {item.name}
                        </Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
}
export default Customer