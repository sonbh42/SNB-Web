import React, { Component } from 'react';
import { Text, View } from 'native-base';
import { TouchableOpacity, StyleSheet,ActivityIndicator } from 'react-native';
export class AuthButton extends Component {
  render() {
    return (
      <TouchableOpacity
        disabled ={this.props.loading}
        onPress={this.props.onPress}
        style={[styles.container, { ...this.props.containerStyle }]}>
        {!this.props.loading ? 
        <Text style={[styles.text, { ...this.props.textStyle }]}>{this.props.text}</Text>
        :<ActivityIndicator size = 'small' color ="#FF5700"/>
        } 
        
      </TouchableOpacity>
    )
  }
}
AuthButton.defaultProps = {
  containerStyle: {},
  textStyle: {},
  loading:false
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    padding: 15,
  },
  text: {
    color: '#F55807'
  }

});
export default AuthButton
