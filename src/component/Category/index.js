import React, { Component } from 'react';
import { Text, View } from 'native-base';


import variables from '../../../theme/variables';
import { createImageLink } from '../../utils'
import images from '../../assets/images';


class Category extends Component {
    static defaultProps = {
        containerStyle: {},
        width: variables.deviceWidth / 3,
    }
    render() {
        return (
            <View style={[{ width: this.props.width }, { ...this.props.containerStyle }]}>
                <Image
                    style={{ width: this.props.width, height: 100 * this.props.width / 115 }}
                    source={this.props.item.image ? { uri: createImageLink('ecommerce', this.props.item.image) }
                        : images.header_drawer} />
                <View full padder white center>
                    <Text size10
                        numberOfLines={2}
                        ellipsizeMode='tail'
                        style={{ color: '#2E3857' }}>
                        {this.props.item.name}
                    </Text>
                </View>
            </View>
        )
    }
}

export default Category