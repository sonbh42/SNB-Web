import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';


class AuthInput extends Component {
    state = {}
    focus() {
        this.input.focus();
    }
    clear() {
        this.input.clear();
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    ref={(ref) => this.input = ref}
                    style={styles.textInput}
                    selectionColor="#FFF"
                    underlineColorAndroid="transparent"
                    keyboardType={this.props.keyboardType}
                    placeholder={this.props.placeholder}
                    placeholderTextColor="white"
                    returnKeyType={"done"}
                    editable={this.props.editable}
                    onSubmitEditing={this.props.onSubmitEditing}
                    onChangeText={this.props.onChangeText}
                    {...this.props}
                />
            </View>
        )
    }
}
AuthInput.defaultProps = {
    editable: true
}
const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        borderBottomColor: "white",
        borderBottomWidth: 1,
    },
    textInput: {
        paddingTop: 15,
        paddingBottom: 15,
        justifyContent: "center",
        alignItems: "center",
        color: "white",
        paddingVertical: 0,
        fontSize: 12,
        fontFamily: 'OpenSans-Light',
    }
});
export default AuthInput