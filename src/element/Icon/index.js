import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { Icon as IconNB, mapPropsToStyleNames, connectStyle } from 'native-base';
import Icomoon, { names } from './icomoon';

connectStyle('NativeBase.Icon', {}, mapPropsToStyleNames)
export default class Icon extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired
  };

  renderIcon() {
    const { name } = this.props;
    // fallback to material?
    return names.indexOf(name) !== -1 ? <Icomoon {...this.props} /> : <IconNB {...this.props} />;
  }

  render() {
    const { onPress } = this.props;
    // if do not have onPress, should make clickable from parent
    return onPress ? (
      <TouchableWithoutFeedback onPress={onPress}>{this.renderIcon()}</TouchableWithoutFeedback>
    ) : (
        this.renderIcon()
      );
  }
}