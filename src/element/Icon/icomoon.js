import {createIconSetFromIcoMoon} from 'react-native-vector-icons'
import config from './selection.json';
const Icomoon = createIconSetFromIcoMoon(config);
export const names =[
    'credit',
    'customer',
    'deal',
    'goods',
    'setup'
    
]
export default Icomoon;