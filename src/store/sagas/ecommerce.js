import { takeLatest, all, put, take, takeEvery } from 'redux-saga/effects';
import {
    APP_GET_LIST_CATEGORY,
    APP_SEARCH_PRODUCT,
    APP_GET_SHOP_CUSTOMER,
} from '../../store/actions/types';
import ecommerce from '../api/ecommerce';
import {
    saveListShopCategory,
    saveListShopProduct,
    saveShopCustomer
} from '../../store/actions/ecommerce'
import { createRequestSaga } from './common';

// const getListShopCategory = createRequestSaga({
//     request: ecommerce.getListShopCategory,
//     key: 'getListShopCategory',
//     success: [res => saveListShopCategory(res.data)],

// });

// const searchProduct = createRequestSaga({
//     request: ecommerce.searchProduct,
//     key: 'searchProduct',
//     success: [
//         res => saveListShopProduct(res.data)
//     ]
// });

const requestGetShopCustomer = createRequestSaga({
    request: ecommerce.getShopCustomer,
    key: 'getShopCustomer',
    success: [
        res => saveShopCustomer(res.data)
    ]
  });
  
export default [

    function* fetchWatcher() {
        yield all([
            // takeLatest(APP_GET_LIST_CATEGORY, getListShopCategory),
            // takeLatest(APP_SEARCH_PRODUCT, searchProduct),
            takeLatest(APP_GET_SHOP_CUSTOMER, requestGetShopCustomer),
         ]);
    }
];
