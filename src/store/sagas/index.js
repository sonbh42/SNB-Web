import {fork,all} from 'redux-saga/effects';
import auth from './auth'
import ecommerce from './ecommerce';
import files from'./files'
const rootSaga = function*(){
    yield all([
        ...auth.map(watcher => fork(watcher)),
        ...ecommerce.map(watcher => fork(watcher)),
        ...files.map(watcher => fork(watcher))
    ]);
};
export default rootSaga;