import {AsyncStorage} from 'react-native';
import {createStore,applyMiddleware,compose} from'redux';
import {persistReducer,persistStore} from 'redux-persist';
import {logger} from 'redux-logger'

import createSagaMiddleware from 'redux-saga';

import reducer from './reducer';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middleware =[sagaMiddleware];
if(process.env.NODE_ENV === 'development'){
    middleware.push(logger);
}
const enhancer = [applyMiddleware(...middleware)];
window.devToolsExtension && enhancer.push(window.devToolsExtension());

const persistConfig = {
    storage :AsyncStorage,
    key :'SNBWeb',
    blacklist :['ui','requests']
}

const persistedReducer = persistReducer(persistConfig,reducer)
const store = createStore(persistedReducer,{},compose(...enhancer));
sagaMiddleware.run(rootSaga);
export const persistor = persistStore(store);

export default store;