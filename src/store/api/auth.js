import { API } from './common';
export default {
    login: (params = {}) => API.post('auths/provider/login', params),
    register: (params={}) => API.post('auths/provider/register', params),
    
    verifyToken: token =>
        API.get(
            'auths/token/verify',
            {},
            { headers: { Authorization: `access_token ${token}` } }
        ),
    
}