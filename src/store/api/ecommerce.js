import { API } from './common';
export default {
    getListShopCategory: (token) =>
        API.get(
            `provider/get/categories`,
            {},
            { headers: { Authorization: `access_token ${token}` } }
        ),
    searchProduct: (token, text, page, quantity) =>
        API.get(
            `product/search?text=${text}&page=${page}&quantity=${quantity}`,
            {},
            { headers: { Authorization: `access_token ${token}` } }
        ),
    getShopCustomer: (token) =>
        API.get(
            `shop/filter/customer`,
            {},
            { headers: { Authorization: `access_token ${token}` } }
        ),
}