import {
    APP_SET_AUTH_STATE,
    APP_SAVE_LOGGED_USER,
    APP_SAVE_REFRESH_TOKEN,
    APP_REMOVE_LOGGED_USER
} from '../actions/types';

const init = {
    loggedIn: false,
    token: null,
    refreshToken: null,
    user: {}
};
export default (state = init, { type, payload }) => {
    switch (type) {
        case APP_SET_AUTH_STATE:
            return { ...state, loggedIn: payload || false };
        case APP_SAVE_LOGGED_USER:
            return {
                ...state,
                user: payload.user,
                token: payload.data.token,
                refreshToken: payload.data.refreshToken
            };
        case APP_REMOVE_LOGGED_USER:
            return { ...state, ...init };
        case APP_SAVE_REFRESH_TOKEN:
            return { ...state, token: { ...state.token, ...payload } }
        default:
            return state;
    }
}