import {combineReducers} from 'redux';
import auth from './auth';
import {requests,toast,drawer,modal} from './common';
export default combineReducers ({
    requests,
    auth,
    ui: combineReducers({
        toast,
        modal,
        drawer,
        
    }),
});