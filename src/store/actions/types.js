/** 
 * AUTH
*/

export const APP_LOGIN ='app/login';
export const APP_LOGOUT ='app/logout';
export const APP_REMOVE_LOGGED_USER = 'app/removeLoggedUser';
export const APP_VERIFY_TOKEN ='app/verifyToken';
export const APP_REGISTER ='app/register';
export const APP_SET_AUTH_STATE ='app/setAuthState';
export const APP_SAVE_LOGGED_USER ='app/saveLoggedUser';
export const APP_SAVE_REFRESH_TOKEN = 'app/saveRefreshToken';

/** 
 * Cart
*/
export const ECOMMERCE_GET_CART = 'cart/getCart';
/**
 * ECOMMERCE
 */
export const APP_GET_SHOP_CUSTOMER = 'ecommerce/getShopCustomer';
export const ECOMMERCE_GET_CATEGORIES = 'ecommerce/getCategories';
export const APP_GET_LIST_CATEGORY = 'ecommerce/getListShopCategory';
export const APP_SAVE_LIST_CATEGORY = 'ecommerce/saveListShopCategory';
export const APP_SAVE_SHOP_CUSTOMER=  'ecommerce/saveShopCustomer';
/**
 * Upload
 */
export const APP_UPLOAD_FILE = 'app/uploadFile';
/**
 * REQUEST
 */
export const MARK_REQUEST_PENDING = 'request/requestPending';
export const MARK_REQUEST_SUCCESS = 'request/requestSuccess';
export const MARK_REQUEST_FAILED = 'request/requestFailed';
export const MARK_REQUEST_CANCELLED = 'request/requestCancelled';
/**
 * INVOKE
 */
export const INVOKE_CALLBACK = 'app/invokeCallBack';
/**
 * TOAST
 */
export const TOAST_SET = 'app/setToast';
export const TOAST_CLEAR = 'app/clearToast';
/**
 * MODAL
 */
export const MODAL_OPEN = 'app/openModal';
export const MODAL_CLOSE = 'app/closeModal';
/**
 * DRAWER
 */
export const DRAWER_OPEN = 'app/openDrawer';
export const DRAWER_CLOSE = 'app/closeDrawer';