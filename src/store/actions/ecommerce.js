import {
    APP_GET_LIST_CATEGORY,
    APP_SAVE_LIST_CATEGORY,
    APP_SEARCH_PRODUCT,
    APP_SAVE_LIST_PRODUCT,

    APP_GET_SHOP_CUSTOMER,
    APP_SAVE_SHOP_CUSTOMER
} from './types'

export const getListShopCategory = (...args) => ({type: APP_GET_LIST_CATEGORY,args});
export const saveListShopCategory = data => ({type: APP_SAVE_LIST_CATEGORY,data });
export const searchProduct  = (...args) => ({type: APP_SEARCH_PRODUCT,args});
export const saveListShopProduct = data => ({type: APP_SAVE_LIST_PRODUCT,payload: data});

export const getShopCustomer  = (...args) => ({type : APP_GET_SHOP_CUSTOMER ,args});
export const saveShopCustomer = data => ({type: APP_SAVE_SHOP_CUSTOMER,payload: data});  
  
