
import config from '../constants/configs';
export const getDomainName = url => {
  let hostname;
  //find & remove protocol (http, ftp, etc.) and get hostname
  if (url.indexOf('://') > -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }
  //find & remove port number
  hostname = hostname.split(':')[0];
  //find & remove "?"
  hostname = hostname.split('?')[0];
  hostname = hostname.replace('www.', '');

  return hostname;
};
export const checkEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
export const checkPassword = value => {
  let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
  return re.test(value);
};
export const createImageLink = (prefix, imageName) => {
  return `${config.endPointImage}/uploads/${prefix}/${imageName}`;
}

export const colorPackageStatus = (status) => {
  //0 cancel, 1 pending, 2 accept, 3 success
  switch (status) {
    case 0: return '#C13838';
    case 1: return '#F5A623';
    case 2: return '#2AA8DB';
    case 3: return '#7DC331';
    default: return 'grey'
  }
}
export const textPackageStatus = (status) => {
  //0 cancel, 1 pending, 2 accept, 3 success
  switch (status) {
    case 0: return 'Đã hủy';
    case 1: return 'Đang chờ';
    case 2: return 'Đang giao hàng';
    case 3: return 'Thành công';
    default: return 'Không xác định'
  }
}