import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { StyleProvider, Root } from 'native-base';

import { Provider } from 'react-redux';
import store from './src/store'
import getTheme from './theme/components';
import material from './theme/variables';

import Home from './router';
import AuthStack from'./router'


class App extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Provider store={store}>
          <Root>
            <StatusBar barStyle='light-content' />
            <AuthStack />
          </Root>
        </Provider>
      </StyleProvider>
    )
  }
}
export default App