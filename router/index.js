import React from 'react';
import {
    createDrawerNavigator,
    createStackNavigator,
    createSwitchNavigator,
    SafeAreaView,
    DrawerItems
} from 'react-navigation';
import {
    YellowBox,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Image,
    TouchableOpacity
} from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import { View, Text, Separator } from 'native-base';

import Icon from '../src/element/Icon';
import PropTypes from 'prop-types';
import Credit from '../src/container/Credit';
import Customer from '../src/container/Customer';
import Deal from '../src/container/Deal';
import Goods from '../src/container/Goods';
import Setup from '../src/container/Setup';
import SlideCustom from '../SlideCustom';
import Login from '../src/container/Auth/Login';
import Register from '../src/container/Auth/Register';
import RegisterStort from '../src/container/Auth/RegisterStort';
import Splash from '../src/container/Splash';
import Search from '../src/container/Search'
import images from '../src/assets/images';

const iconDrawer = (name) => ({ tintColor, focused }) => (
    <Icon name={name} color={focused ? '#F55807' : '#83889F'} style={{ fontSize: 21, marginBottom: 5, marginTop: 5, }} size={21} />
);
const labelDrawer = (label) => ({ tintColor, focused }) => (
    <Text size12 style={{ color: focused ? '#F55807' : '#83889F', marginBottom: 5, marginTop: 5 }}>{label}</Text>
);

const CustomDrawerContentComponent = (props) => (
    <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
            <View style={{ height: 200 }}>
                <ImageBackground source={images.header_drawer} style={styles.imground}>
                    <View flexStart>
                        <Image style={styles.image} source={images.demo_avatar} />
                        <Text size10 style={{ marginTop: 12 }}>Email: twinger1@gmail.co </Text>
                    </View>
                </ImageBackground>


            </View>
            <Separator bordered>
                <Text>MIDFIELD</Text>
            </Separator>
            <DrawerItems {...props} />
            <Separator style={{ height: 10 }}>

            </Separator>

        </SafeAreaView>
    </ScrollView >

);
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    imground: {
        width: null,
        height: null,
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 40
    }


});
const AuthStack = createStackNavigator({
    Login: { screen: Login },
    Register: { screen: Register },
    RegisterStort: { screen: RegisterStort },

}, {
        navigationOptions: {
            header: null,
        }
    })

const Home = createDrawerNavigator({
    Credit: {
        screen: Credit,
        navigationOptions: {
            drawerLabel: labelDrawer('Quản lý tín dụng'),
            drawerIcon: iconDrawer('credit'),
        }
    },
    Customer: {
        screen: Customer,
        navigationOptions: {
            drawerLabel: labelDrawer('Khách hàng'),
            drawerIcon: iconDrawer('customer'),
        }
    },
    Deal: {
        screen: Deal,
        navigationOptions: {
            drawerLabel: labelDrawer("Giao dịch"),
            drawerIcon: iconDrawer('deal'),

        }
    },
    Goods: {
        screen: Goods,
        navigationOptions: {
            drawerLabel: labelDrawer('Quản lý hàng hóa'),
            drawerIcon: iconDrawer('goods')
        }
    },
    Setup: {
        screen: Setup,
        navigationOptions: {
            drawerLabel: labelDrawer('Thiết lập'),
            drawerIcon: iconDrawer('setup')
        }
    }

}, {

        contentComponent: CustomDrawerContentComponent,
        // contentComponent: props=><SlideCustom{...props}/>,
        initialRouteName: 'Customer',
        useNativeAnimations: false,
        contentOptions: {
            itemsContainerStyle: {
                // marginTop: 30,
            },
            activeBackgroundColor: 'white'

        },


    });
const AppDrawer = createStackNavigator({
    Home: { screen: Home },
    Search : {screen :Search}


}, {
        navigationOptions: {
            header: null
        }
    });
const AppNavigator = createSwitchNavigator({
    Splash: { screen: Splash },
    AuthStack: { screen: AuthStack },
    AppDrawer: { screen: AppDrawer },


}, {
        initialRouteName: 'Splash'
    })

export default AppNavigator;